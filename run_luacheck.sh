#! /bin/bash


for f in mods/*; do

   if [ "$f" = "mods/mobkit" ] ; then
       continue
   fi
   if [ "$f" = "mods/mob_core" ] ; then
       continue
   fi
   if [ "$f" = "mods/central_message" ] ; then
       continue
   fi
   pushd "$f"
   luacheck .
   popd > /dev/null

done
