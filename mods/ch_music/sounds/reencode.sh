#!/bin/sh
set -ex

find . -name '*.ogg' |\
while read X; do
        ogginfo "$X" | perl -ne '
                m#Nominal\s+bitrate:\s*([\d\.]+)\s*kb/s#i
                and $1 <= 160 and exit 1
        ' || continue
        sox "$X" -t wav - | oggenc -q 5 - >.tmp
        mv -f .tmp "$X"
done

